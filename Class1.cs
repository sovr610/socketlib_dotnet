﻿using System;
using System.Threading;
using AwesomeSockets.Domain.Sockets;
using AwesomeSockets.Sockets;
using Buffer = AwesomeSockets.Buffers.Buffer;

namespace SocketLib
{
    public class ServerCode
    {
        private Func<ISocket, string> delegate_get;

        private Func<ISocket, string, int> delegate_send;
        private readonly ISocket server;

        public ServerCode(int port, Func<ISocket, string, int> send, Func<ISocket, string> get)
        {
            server = AweSock.TcpListen(port);
            while (true)
            {
                var client = AweSock.TcpAccept(server);
                var _extra = new ServerCode();
                _extra.delegate_get = get;
                _extra.delegate_send = send;
                var needle = new Thread(_extra.handleClient);
                needle.Start(client);
            }
        }

        public ServerCode()
        {
        }

        public bool setSendDeletegate(Func<ISocket, string, int> func)
        {
            delegate_send = func;
            return true;
        }

        public bool setGetDelegate(Func<ISocket, string> func)
        {
            delegate_get = func;
            return true;
        }


        public void handleClient(object client)
        {
            var _client = (ISocket) client;
            var s = _client.GetSocket();
            while (true)
            {
                if (!s.Connected)
                {
                    s.Close();
                    break;
                }

                try
                {
                    var info = delegate_get((ISocket) client);
                    delegate_send((ISocket) client, info);
                }
                catch (Exception i)
                {
                    Console.WriteLine(i.Message);
                    break;
                }
            }
        }

        public void sendInfo(ISocket client, string content)
        {
            delegate_send(client, content);
        }

        public string getInfo(ISocket client)
        {
            return delegate_get(client);
        }
    }

    public class ClientCode
    {
        private readonly ISocket client;

        public ClientCode(string ip, int port)
        {
            try
            {
                client = AweSock.TcpConnect(ip, port);
            }
            catch (Exception i)
            {
            }
        }

        public int sendInfo(string message)
        {
            var inBuf = Buffer.New();
            Buffer.ClearBuffer(inBuf);
            Buffer.Add(inBuf, message);
            Buffer.FinalizeBuffer(inBuf);

            var bytesSent = AweSock.SendMessage(client, inBuf);
            return bytesSent;
        }

        public Buffer getInfo()
        {
            var _outBuffer = Buffer.New();
            var received = AweSock.ReceiveMessage(client, _outBuffer);
            return _outBuffer;
        }
    }
}